<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api/v1'], function () use ($router) {

    $router->get('/', function () use ($router) {
        return $router->app->version();
    });

    /*
    |--------------------------------------------------------------------------
    | Максимально 5 попыток в 180 секунд для одного ip
    |--------------------------------------------------------------------------
    */
    $router->group(['middleware' => 'throttle:5,180'], function () use ($router) {
        $router->post('register', 'AuthController@register');
        $router->post('login', 'AuthController@login');
    });


    /*
    |--------------------------------------------------------------------------
    | Защищенные маршруты
    |--------------------------------------------------------------------------
    */
    $router->group(['middleware' => 'auth'], function () use ($router) {

        $router->get('code', 'MailController@code');

    });
});
